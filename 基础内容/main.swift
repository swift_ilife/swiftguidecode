//
//  main.swift
//  基础内容
//
//  Created by xiao on 2019/5/23.
//  Copyright © 2019 xiao. All rights reserved.
//

import Foundation

print("基础内容")

print("-------常量和变量--------")


//常量和变量必须在使用前被声明，使用关键字 let 来声明常量，使用关键字 var 来声明变量
//行末可以写分号,可以不写分号
//同一行有2局代码,必须写分号
let a = 10
print("\(a)")

var b = 20;
print("\(b)")
b = 30 ; print("\(b)")


print("------类型标注---------")

//类型标注:在变量或常量的名字后边加一个冒号，再跟一个空格，最后加上要使用的类型名称
//声明字符串,不用再加 @ 符了
//你并不需要经常使用类型标注 ,因为Swift 有类型推断
var title: String
title = "hello"
print("\(title)")

//同时声明多个变量
var aa, bb, cc, dd: Int


print("------命名常量和变量---------")

let π = 3.12159
let 你好 = "你好,世界"
//let ?? = "dogC"

//如果你需要使用 Swift 保留的关键字来给常量或变量命名，可以使用反引号（ ` ）包围它来作为名称
var `var` = "呵呵"

print("\(π)")
print("\(你好)")
print("\(`var`)")

print("------注释---------")
//单行注释
/*多行注释*/
/*
 嵌套注释
/*
 嵌套注释
 */
 */

print("------整数范围---------")
//Swift 提供了 8，16，32 和 64 位编码的有符号和无符号整数
let num = UInt8.min
let num2 = UInt8.max
print("UInt8的最小值:\(num)")
print(("UInt8的最大值:\(num2)"))
print("UInt16的最小值:\(UInt16.min)")
print(("UInt16的最大值:\(UInt16.max)"))
print("UInt32的最小值:\(UInt32.min)")
print(("UInt32的最大值:\(UInt32.max)"))
print("UInt64的最小值:\(UInt64.min)")
print(("UInt64的最大值:\(UInt64.max)"))
print("------------------------------")
print("Int8的最小值:\(Int8.min)")
print(("Int8的最大值:\(Int8.max)"))
print("Int16的最小值:\(Int16.min)")
print(("Int16的最大值:\(Int16.max)"))
print("Int32的最小值:\(Int32.min)")
print(("Int32的最大值:\(Int32.max)"))
print("Int64的最小值:\(Int64.min)")
print(("Int64的最大值:\(Int64.max)"))
//注意大写  在32位平台上， Int 的长度和 Int32 相同。在64位平台上， Int 的长度和 Int64 相同。
let num3: Int = 23

//不能用类型直接声明变量了,要用类型标注的方式 (以下写法会报错)
//Int num4 = 33


print("------类型安全和类型推断---------")

//Double代表 64 位的浮点数。Float 代表 32 位的浮点数。
//如果你在一个表达式中将整数和浮点数结合起来， Double 会从内容中被推断出来。--->类型优先级(c)
let num5 = 3 + 3.233

print("------数值型字面量--(进制表达式)-------")

//数字 17 的几种不同进制的写法
let decimalInteger = 17 //十进制
let binaryInteger = 0b10001 // 17 二进制
let octalInteger = 0o21 // 17 八进制
let hexadecimalInteger = 0x11 // 17 十六进制

//指数
//十进制数与 exp  的指数，结果就等于基数乘以 10exp：
//字面量为: 1.25 * 10的平方
let num7  = 1.25e2;
print("\(num7)")

//十六进制的浮点字面量必须有指数，用大写或小写的 p 来表示
//字面量为: 1.25 *2的平方
let num8 = 0xffp2;
print("\(num8)")

print("------整数转换---------")
let n1: UInt8 = 2;
let n2: UInt16 = 44;
//他们不能直接被相加在一起，因为他们的类型不同,转换后:两边的类型现在都是  UInt16 ，所以现在是可以相加的
let n3 = UInt16(n1) + n2 ;
print("n3 = \(n3)")

let n4: Int = 5
let n5: Double = 17.2;
let n7 = Double(n4) + n5
print("n7 = \(n7)")

print("------类型别名---------")

typealias CustomInt = UInt16
print("\(CustomInt.min)")
print("\(CustomInt.max)")

print("---------布尔值--------")
//基础的布尔量类型，就是 Bool ，布尔量被作为逻辑值来引用
//提供了两个常量值， true 和 false
let oo = true
let bbc = false
if oo {
    print("Mmm, tasty turnips!")
} else {
    print("Eww, turnips are horrible.")
}

//条件返回必须是一个bool值
//示例 :错误写法
/*
let i = 1
if i {
    // this example will not compile, and will report an error
}
 */

//正确写法
let i = 1
if i == 1 {
    // this example will not compile, and will report an error
}

print("---------元组-----------")
/*
 1.元组把多个值合并成单一的复合型的值
 2.元组内的值可以是任何类型，而且可以不必是同一类型
 */

// (Int, String) 类型的元组 -- 类型推断
let http404erro = (404,"error info")
print("元组输出 : \(http404erro)")

//元组取值 - 通过下标取值
print("元组第0个元素: \(http404erro.0)")
print("元组第1个元素: \(http404erro.1)")

//通过变量取值
let (statusCode,errorMessage) = http404erro
print("元组通过变量取值 :\(statusCode)")
print("元组通过变量取值 :\(errorMessage)")

//你分解元组的时候，如果只需要使用其中的一部分数据，不需要的数据可以用 下滑线（ _ ）代替
//定义的变量也是私有变量,不能和其他变量名重复
let (_,errorMessage2) = http404erro
print("元组通过变量取值 :\(errorMessage2)")

//定义元组的时候就起好变量名
let http200Status = (successCode:200,successMessage:"请求成功")
print("http200Status :\(http200Status.successCode)")
print("http200Status :\(http200Status.successMessage)")



print("---------可选项 = 空值判断-----------")
/*
 1.在 C 和 Objective-C 中，没有可选项的概念
 2.oc中相近的概念为nil,但是只能 作用于 对象上,不能作用于结构体
 3.Objective-C 会返回一个特殊的值（例如 NSNotFound ）来表示值的缺失
 4.swift 中的可选项就可以让你知道任何类型的值的缺失,不需要一个特殊的值
 */
//会报警告的写法 :
//let strNum = "123"
//let coverNum = Int(strNum)
//print("coverNum = \(coverNum)")

//Int(strNum) 可能转化成功,也可能不成功,所以 coverNum可能有值,也可能没有值,返回的是一个 Int? 可选类型,而不是 Int


print("---------空类型 :nil-----------")
/*
1.你可以通过给可选变量赋值一个 nil 来将之设置为没有值
2.在 Objective-C 中 nil 是一个指向不存在对象的指针
3.Swift中， nil 不是指针，他是值缺失的一种特殊类型，任何类型的可选项都可以设置成 nil 而不仅仅是对象类型
*/

var serverResponseCode: Int? = 404
serverResponseCode = nil

//nil 不能用于非可选的常量或者变量
//错误写法示例

/*
    var name = "xiao"
    name = nil
 */

//正确写法示例
var name: String? = "xiao"
name = nil

//如果你定义的可选变量没有提供一个默认值，变量会被自动设置成 nil
var anyThing :String?
print("没有初始化的默认值 = \(anyThing)")


print("---------强制展开 ! = 确定有值-----------")
let strNum1 = "123"
let coverNum1 = Int(strNum1)
//知道"123"肯定可以转化成功,肯定有值
print("coverNum1 = \(coverNum1!)")


print("---------可选绑定-----------")
//如果Int(strNum1) 有值,将会赋值给 possibleNum 这个变量
if let possibleNum = Int(strNum1){
    print("possibleNum = \(possibleNum)")
}

//在同一个 if 语句中包含多可选项绑定，用逗号分隔即可
//如果任一可选绑定结果是 nil 或者布尔值为 false ，那么整个 if 判断会被看作 false----->有警告,推断可选绑定中,元组的取值存在类型推断的问题.
/*
if let code:Int? = http404erro.0 ,let message:String? = http404erro.1 {
    print("code = \(code),message = \(message)")
}
*/
if let firstNumber = Int("4"), let secondNumber = Int("42"), firstNumber < secondNumber && secondNumber < 100 {
    print("\(firstNumber) < \(secondNumber) < 100")
}

//相当于:
if let firstNumber = Int("4") {
    if let secondNumber = Int("42") {
        if firstNumber < secondNumber && secondNumber < 100 {
            print("\(firstNumber) < \(secondNumber) < 100")
        }
    }
}

/*
 有时在一些程序结构中可选项一旦被设定值之后，就会一直拥有值。在这种情况下，就可以去掉检查的需求，也不必每次访问的时候都进行展开，因为它可以安全的确认每次访问的时候都有一个值
 如果你在隐式展开可选项没有值的时候还尝试获取值，会导致运行错误
 */

let name3: String? = "lili"
print("name3 = \(name3!)")

//如果确定有值,就可以不用展开
//你可以把隐式展开可选项当做在每次访问它的时候被给予了自动进行展开的权限，你可以在声明可选项的时候添加一个叹号而不是每次调用的时候在可选项后边添加一个叹号。
let name4: String! = "An implicitly unwrapped optional string"
let name5 = name4;

print("name4 = \(name4!)")



print("---------错误处理--------")
//错误类型
enum Error{
    case OutOfCleanDishes
    case MissingIngredients
}

//定义一个会抛出错误的函数
func makeASandwich() throws {
    // ...
}

func eatASandwich(){
    //没有抛出错误的时候的执行函数
}

func washDishes(){
    //错误执行函数一
}

func buyGroceries(){
    //错误执行函数二
}

//do 定义了一个新的容器范围,可以让错误被传递到到不止一个的 catch 分句里。
do {
    //当你调用了一个可以抛出错误的函数时，需要在表达式前预置 try 关键字
    try makeASandwich()
    eatASandwich()
} catch Error.OutOfCleanDishes {
    washDishes()
} catch Error.MissingIngredients {
    buyGroceries()
}

print("---------断言和先决条件--------")
/*
 1.如果布尔条件在断言或先决条件中计算为 true ，代码就正常继续执行。如果条件计算为 false ，
 那么程序当前的状态就是非法的；代码执行结束，然后你的 app 终止
 
 2.断言和先决条件的不同之处在于他们什么时候做检查：断言只在 debug 构建的时候检查，但先决条件则在 debug 和生产构建中生效。
 在生产构建中，断言中的条件不会被计算。这就是说你可以在开发的过程当中随便使用断言而无需担心影响生产性能
 
 3.你可以使用全局函数 assert(_:_:)  函数来写断言
 */

let age = -3;
//assert(age >= 0 ,"一个人的年龄必须大于等于0")
//断言信息可以删掉
//assert(age >= 0)



//如果代码已经检查了条件，你可以使用 assertionFailure(_:file:line:) 函数来标明断言失败

if age > 10 {
    print("You can ride the roller-coaster or the ferris wheel.")
} else if age > 0 {
    print("You can ride the ferris wheel.")
} else {
//    assertionFailure("年龄必须>=0")
}

print("---------强制先决条件--------")


//在你代码中任何条件可能潜在为假但必须肯定为真才能继续执行的地方使用先决条件
//precondition(index > 0, "Index must be greater than zero.")
