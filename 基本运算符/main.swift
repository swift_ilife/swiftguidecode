//
//  main.swift
//  基本运算符
//
//  Created by xiao on 2019/5/23.
//  Copyright © 2019 xiao. All rights reserved.
//

import Foundation
print("------------赋值运算符----------")
let bb = 10
var aa = 5
aa = bb

//如果赋值符号右侧是拥有多个值的元组，它的元素将会一次性地拆分成常量或者变量
let (x, y) = (1, 2)
// = 没有返回值,
//if x = y {
//    // 这是不合法的, 因为 x = y 并不会返回任何值。
//}



print("------------算术运算符----------")
/*
 加 ( + )
 减 ( - )
 乘 ( * )
 除 ( / )
 */

//加法运算符同时也支持 String  的拼接
"hello, " + "world" // equals "hello, world"




print("------------取余运算符----------")


let a = 9 % 4
let b = 9 % -4
let c = -9 % 4
let d = -9 % -4
print("a = \(a), b = \(b),c = \(c), d = \(d)")//a = 1, b = 1,c = -1, d = -1


print("------------组合运算符----------")
var a1 = 1
a1 += 2

print("------------比较运算符----------")

/*
 相等 ( a == b )
 不相等 ( a != b )
 大于 ( a > b )
 小于 ( a < b )
 大于等于 ( a >= b )
 小于等于 ( a <= b )
 */


print("------------三目运算符----------")

let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20)


print("------------合并空值运算符----------")
/*
 是下面的简写
 如果可选项 a  有值则展开，如果没有值，是 nil  ，则返回默认值 b
 表达式 a 必须是一个可选类型。表达式 b  必须与 a  的储存类型相同。
 */

//a != nil ? a! :b
let redColor  = "red"
var defaultColor :String?
var color = defaultColor ?? redColor;
print("color = \(color)")



print("------------区间运算符----------")

//闭区间 ,包含 a b ,不能大于b
a...b

//半开半闭区间,定义了从 a  到 b  但不包括 b  的区间
//半开区间在遍历基于零开始序列比如说数组的时候非常有用
a..<b

//单侧区间
2...

...5
//可以检测单侧区间是否包含特定的值
let range = ...10
range.contains(7);
range.contains(20);
range.contains(-4)


print("------------逻辑运算符----------")
/*
 逻辑 非  ( !a )
 逻辑 与  ( a && b )
 逻辑 或  ( a || b )
 */
let enteredDoorCode = true
let passedRetinaScan = false
let hasDoorKey = true;
let knowsOverridePassword = true

if (enteredDoorCode && passedRetinaScan) || hasDoorKey || knowsOverridePassword {
    print("Welcome!")
} else {
    print("ACCESS DENIED")
}
