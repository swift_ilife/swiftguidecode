//
//  main.swift
//  字符串和字符
//
//  Created by xiao on 2019/5/23.
//  Copyright © 2019 xiao. All rights reserved.
//

import Foundation

print("Hello, World!")

//Swift 的 String类型桥接到了基础库中的 NSString类。Foundation 同时也扩展了所有 NSString 定义的方法给 Strin
//字符串串联只需要使用 +运算符即可

//多行字符标示方法
let quotation = """
The White Rabbit put on his spectacles.  "Where shall I begin,
please your Majesty?" he asked.

"Begin at the beginning," the King said gravely, "and go on
till you come to the end; then stop."
"""

//转义字符
//要在多行字符串中包含 """ ，你必须用反斜杠（ \ ）转义 至少其中一个 双引号
let threeDoubleQuotes = """
Escaping the first quote \"""
Escaping all three quotes \"\"\"
"""
print("\(threeDoubleQuotes)")


//初始化
var string1 = "";
var string2 = String();

//string的空值判断
if string1.isEmpty{
    print("sting1 is empty");
}

//字符串拼接
var string3 = "heheda"
string3 += " ma mai pi"
print("\(string3)")

//Swift 的 String类型是一种值类型
//每一次赋值和传递，现存的 String值都会被复制一次，传递走的是拷贝而不是原本
//Swift 编译器优化了字符串使用的资源，实际上拷贝只会在确实需要的时候才进行

//字符串遍历
for c in "dog!?"{
    print("\(c)")
}


//通过类型标注来显示 创建字符
//你使用 String类型的 append()方法来可以给一个 String变量的末尾追加 Character值

let t : Character = "!"
string3.append(t)

//通过字符数组来初始化字符串
// = 号 两边强制留空格
let tArray: [Character] = ["a","r","r","a","y"]
let tSting = String(tArray)
print("字符数组->初始化字符串  \(tSting)")


//转义特殊字符 \0 (空字符)， \\ (反斜杠)， \t (水平制表符)， \n (换行符)， \r(回车符)， \" (双引号) 以及 \' (单引号)

let enclosedEAcute: Character = "\u{E9}\u{20DD}"
print("封闭字符标量 : \(enclosedEAcute)")

//字符统计 count

print("字符的长度统计 :\(string3.count)")


/*
 扩展字形集群能够组合一个或者多个 Unicode 标量。这意味着不同的字符——以及相同字符的不同表示——能够获得不同大小的内存来储存。
 因此，Swift 中的字符并不会在字符串中获得相同的内存空间。所以说，字符串中字符的数量如果不遍历它的扩展字形集群边界的话，是不能被计算出来的。
 如果你在操作特殊的长字符串值，要注意 count属性为了确定字符串中的字符要遍历整个字符串的 Unicode 标量。
 
 通过 count属性返回的字符统计并不会总是与包含相同字符的 NSString中 length属性相同。
 NSString中的长度是基于在字符串的 UTF-16 表示中16位码元的数量来表示的，而不是字符串中 Unicode 扩展字形集群的数量。
 */





//Swift 的字符串不能通过整数值索引
//使用 startIndex属性来访问 String中第一个 Character的位置
//endIndex属性就是 String中最后一个字符后的位置
//使用 index(before:) 和 index(after:) 方法来访问给定索引的前后

let string4 = "hello bcd";
print("第一个字符 : \(string4[string4.startIndex])")
//print("最后一个字符 : \(string4[string4.endIndex])")
print("最后一个字符: \(string4[string4.index(before: string4.endIndex)])") //最后一个字符的正确访问方式
print("第二字符: \(string4[string4.index(after: string4.startIndex)])")

//从0开始数的坐标
print("第4个字符 : \(string4[string4.index(string4.startIndex, offsetBy: 4)])") //o
print("第n个字符 : \(string4[string4.index(string4.endIndex, offsetBy: -3)])") //o


//尝试访问index之外的索引会引发错误
//print("最后一个字符 : \(string4[string4.endIndex])") //error
//greeting.index(after: endIndex) // error

//通过索引来方位每一个字符
for i in string4.indices{
    print(string4[i])
}

//MARK - 插入和删除
//你可以在任何遵循了 RangeReplaceableIndexable 协议的类型中使用 insert(_:at:) ，
//insert(contentsOf:at:) ， remove(at:) 方法。这包括了这里使用的 String ，
//同样还有集合类型比如 Array ， Dictionary 和 Set 。

var testSting = "hello boy"
//插入一个字符
testSting.insert("e", at: testSting.endIndex)
print(testSting)

//移除一个字符
testSting.remove(at: testSting.index(before: testSting.endIndex))
print(testSting)


//插入一个字符串
testSting.insert(contentsOf: " girl", at: testSting.endIndex)
print(testSting)

//移除一段区间的字符
let range = testSting.index(testSting.endIndex, offsetBy: -4) ..< testSting.endIndex
testSting.removeSubrange(range)
print(testSting)


//MARK - 子字符串,内存共享
let string6 = "hello,world"
//fuck! 方法没有提示
//let index = string6.index(of: ",") ?? string6.endIndex
let index = string6.firstIndex(of: ",") ?? string6.endIndex

let subString = string6[..<index]
print(subString)

//子串 转 字符串
let string7 = String(subString)
print(string7)

//MARK Swift 提供了三种方法来比较文本值：字符串和字符相等性，前缀相等性以及后缀相等性。

let str1 = "hello world"
let str2 = "hello world"
if str1 == str2 {
    print("the two string are considered equal")
}

// 规范化相等 了解即可

//两个 String值（或者两个 Character值）如果它们的扩展字形集群是规范化相等，则被认为是相等的。如果扩展字形集群拥有相同的语言意义和外形，我们就说它规范化相等，就算它们实际上是由不同的 Unicode 标量组合而成。
// "Voulez-vous un café?" using LATIN SMALL LETTER E WITH ACUTE
let eAcuteQuestion = "Voulez-vous un caf\u{E9}?"

// "Voulez-vous un café?" using LATIN SMALL LETTER E and COMBINING ACUTE ACCENT
let combinedEAcuteQuestion = "Voulez-vous un caf\u{65}\u{301}?"

if eAcuteQuestion == combinedEAcuteQuestion {
    print("These two strings are considered equal")
}

//反而， LATIN CAPITAL LETTER A ( U+0041, 或者说 "A")在英语当中是不同于俄语的 CYRILLIC CAPITAL LETTER A ( U+0410,或者说 "А")的。字符看上去差不多，但是它们拥有不同的语言意义：
let latinCapitalLetterA: Character = "\u{41}"

let cyrillicCapitalLetterA: Character = "\u{0410}"

if latinCapitalLetterA != cyrillicCapitalLetterA {
    print("These two characters are not equivalent")
}


//要检查一个字符串是否拥有特定的字符串前缀或者后缀，调用字符串的 hasPrefix(_:)和 hasSuffix(_:)方法，
//它们两个都会接受一个 String 类型的实际参数并且返回一个布尔量值
let romeoAndJuliet = [
    "Act 1 Scene 1: Verona, A public place",
    "Act 1 Scene 2: Capulet's mansion",
    "Act 1 Scene 3: A room in Capulet's mansion",
    "Act 1 Scene 4: A street outside Capulet's mansion",
    "Act 1 Scene 5: The Great Hall in Capulet's mansion",
    "Act 2 Scene 1: Outside Capulet's mansion",
    "Act 2 Scene 2: Capulet's orchard",
    "Act 2 Scene 3: Outside Friar Lawrence's cell",
    "Act 2 Scene 4: A street in Verona",
    "Act 2 Scene 5: Capulet's mansion",
    "Act 2 Scene 6: Friar Lawrence's cell"
]

var account = 0
for sence in romeoAndJuliet{
    if sence.hasPrefix("Act 1"){
        account+=1;
    }
}
print(account)

var endCount1 = 0
var endCount2 = 0
for sence in romeoAndJuliet {
    if sence.hasSuffix("Capulet's mansion"){
        endCount1 += 1
    }else if(sence.hasSuffix("Friar Lawrence's cell")){
        endCount2 += 1
    }
}
print("后缀相等性 :\(endCount1) & \(endCount2)")

//unicode 表示法不常用,如有需要参考官方文档
