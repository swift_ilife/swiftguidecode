//
//  main.swift
//  集合类型
//
//  Created by xiao on 2019/5/23.
//  Copyright © 2019 xiao. All rights reserved.
//

import Foundation

//Swift 的数组、合集和字典是以泛型集合实现的。要了解更多关于泛型类型和集合，参见泛型
//var 如果你创建一个数组、合集或者一个字典，并且赋值给一个变量 可变
//let 如果你创建一个数组、合集或者一个字典，并且赋值给一个常量 不可变
//Swift 的 Array类型被桥接到了基础框架的 NSArray类上
//Swift 数组的类型完整写法是 Array<Element>， Element是数组允许存入的值的类型
//你同样可以简写数组的类型为 [Element]

var someInt = [Int]()

//如果内容已经提供了类型信息，比如说作为函数的实际参数或者已经分类了的变量或常量，你可以通过空数组字面量来创建一个空数组
someInt = []

//Array类型提供了初始化器来创建确定大小且元素都设定为相同默认值的数组
var nameArry = Array(repeating: 33.2, count: 5) //["name", "name", "name", "name", "name"]
print(nameArry)


//通过连接两个数组来创建数组
var nameArry2 = Array(repeating: 2.5, count: 3)
var name3 = nameArry + nameArry2 //同类型的数组可以直接相加
print(name3)

//使用字面量创建数组
var names :[String] = ["lili","lucy"]
print(names)

// 数组数量
print("names 元素数量为 :\(names.count)")

//末尾拼接一个元素
names.append("jack")
print(names)

//+ 号 末尾拼接
names += ["jimi"]
print(names)

//拼接多个元素
names += ["jim","bluce"]
print(names)


//下标访问元素
print(names[0])

//修改特定位置的元素
names[0] = "tom"
print(names)

//你同样可以使用下标脚本语法来一次改变一个范围的值
names [..<2] = ["boy","girl"]
print(names)

//插入元素 (在特定位置)
names.insert("china", at: 0)
print(names)

//移除特定位置元素
names.remove(at: 0)
print(names)

//值得注意的是,移除的方法会将移除的元素返回,如果需要的话,可以定义一个常量来接收
let mapleSyrup = names.remove(at: 0)
print(names)
print(mapleSyrup)//boy

//最大的合法索引永远都是 count - 1，因为数组的索引从零开始。

//使用 removeLast()方法而不是 remove(at:)方法以避免查询数组的 count属性
let lastName = names.removeLast()
print(lastName)
print(names)


//遍历数组

for item in names {
    print(item)
}

//如果你需要每个元素以及值的整数索引，使用 enumerated()方法来遍历数组。 enumerated()方法返回数组中每一个元素的元组

for (index,value) in names.enumerated(){
    print((index,value))
}


//set 合集将同一类型且不重复的值无序地储存在一个集合当中 ,它必须是可哈希的
//所有 Swift 的基础类型（比如 String, Int, Double, 和 Bool）默认都是可哈希的，并且可以用于合集或者字典的键
//没有关联值的枚举成员值（如同枚举当中描述的那样）同样默认可哈希。

/*
 你可以使用你自己自定义的类型作为合集的值类型或者字典的键类型，只要让它们遵循 Swift 基础库的 Hashable协议即可。遵循 Hashable协议的类型必须提供可获取的叫做 hashValue的 Int属性。通过 hashValue属性返回的值不需要在同一个程序的不同的执行当中都相同，或者不同程序。
 
 因为 Hashable协议遵循 Equatable，遵循的类型必须同时一个“等于”运算符 ( ==)的实现。 Equatable协议需要任何遵循 ==的实现都具有等价关系。就是说， ==的实现必须满足以下三个条件，其中 a, b, 和 c是任意值：
 
 a == a  (自反性)
 a == b 意味着 b == a  (对称性)
 a == b && b == c 意味着 a == c  (传递性)

 
 */

//合集类型语法 Set<Element>
//集合初始化
//使用初始化器语法来创建一个确定类型的空合集：
var letters = Set<Character>()
print("letters is of type Set<Character> with \(letters.count) items.")

//比如函数的实际参数或者已经分类的变量常量，你就可以用空的数组字面量来创建一个空合集：
letters = [];

//用数组字面量来初始化 合集
//合集类型不能从数组字面量推断出来，所以 Set类型必须被显式地声明。
var letters2 :Set<String> = ["Rock", "Classical", "Hip hop"]

//元素数量
print("I have \(letters2.count) favorite music genres.")

if letters2.isEmpty{
    print("集合是空的")
}else{
    print("集合不是空的 :\(letters2)")
}

//添加元素

letters2.insert("apple")
print(letters2)

//remove 会返回要移除的元素
//如果元素是合集的成员就移除它，并且返回移除的值，如果合集没有这个成员就返回 nil。
if let fruit = letters2.remove("apple"){
    print("移除的对象为:\(fruit)")
}else{
    print("么有这个元素)")
}

//要检查合集是否包含了特定的元素，使用 contains(_:)方法
if letters2.contains("Rock"){
    print("集合中包含Rock这个元素")
}else{
    print("集合中不包含这个元素")
}

//遍历结合
//遍历后无序返回
for c in letters2{
    print(c)
}
print("------")
//遍历后有序返回 --> 它把合集的元素作为使用 < 运算符排序了的数组返回
for c in letters2.sorted(){
    print(c)
}

//集合操作
var set1: Set = [1, 3, 5, 7, 9]
var set2: Set = [0, 2, 4, 6, 8]
var set3: Set = [2, 3, 5, 7]

//并集 包含两个合集所有值的新合集
var set4 = set1.union(set2).sorted()
print(set4)

//交集 只包含两个合集共有值的新合集
let set5 = set1.intersection(set2)
print(set5)

//两个合集当中不包含某个合集值的新合集(set1 - 交集)
let set6 = set1.subtracting(set3)
print(set6)

//含两个合集各自有的非共有值的新合集 (并集-交集)
let set7 = set1.symmetricDifference(set3).sorted()
print(set7)


/*
 使用“相等”运算符 ( == )来判断两个合集是否包含有相同的值；
 使用 isSubset(of:) 方法来确定一个合集的所有值是被某合集包含；
 使用 isSuperset(of:)方法来确定一个合集是否包含某个合集的所有值；
 使用 isStrictSubset(of:) 或者 isStrictSuperset(of:)方法来确定是个合集是否为某一个合集的子集或者超集，但并不相等；
 使用 isDisjoint(with:)方法来判断两个合集是否拥有完全不同的值。
 */


let houseAnimals: Set = ["?", "?"]
let farmAnimals: Set = ["?", "?", "?", "?", "?"]
let cityAnimals: Set = ["?", "?"]

let result1 :Bool =  houseAnimals.isSubset(of: farmAnimals)
print(result1)
// true
let result2 :Bool = farmAnimals.isSuperset(of: houseAnimals)
print(result2)
// true
let result3 :Bool = farmAnimals.isDisjoint(with: cityAnimals)
print(result3)
// true

//字典 Swift 的字典类型写全了是这样的： Dictionary<Key, Value>
//初始化
var dict = [Int :String]()

//如果内容已经提供了信息，你就可以用字典字面量创建空字典了，它写做 [:]（在一对方括号里写一个冒号）
dict[16] = "sixteen"
// namesOfIntegers now contains 1 key-value pair
dict = [:]
// namesOfIntegers is once again an empty dictionary of type [Int: String]

//用字面量来创建字典
//它意思是“一个键和值都是 String的 Dictionary
var dict2 :[String:String] = ["city":"beijing","address":"ChaoYang"]

//如果你用一致类型的字典字面量初始化字典，就不需要写出字典的类型了
var airports = ["YYZ": "Toronto Pearson", "DUB": "Dublin"]

//字典的为空判断

if dict.isEmpty{
    print("字典为空")
}






